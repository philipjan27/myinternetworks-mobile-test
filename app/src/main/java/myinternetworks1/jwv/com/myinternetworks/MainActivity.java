package myinternetworks1.jwv.com.myinternetworks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLogin= (Button) findViewById(R.id.btnLogin);
        Button btnRegister= (Button) findViewById(R.id.btnRegister);
        TextView tvDescription= (TextView) findViewById(R.id.tvDescription);

        tvDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "This Application is created by JWM Inc. ",Toast.LENGTH_LONG).show();
            }
        });
    }
}
